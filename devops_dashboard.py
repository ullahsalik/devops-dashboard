import tornado.web
import tornado.ioloop
from handlers import *

def make_app():
    return tornado.web.Application([
        (r"/", IndexHandler),
        (r"/cronstats", CronHandler),
        (r"/gitstats", GitHandler),
        (r"/form", FormHandler),
        (r"/images/(.*)", tornado.web.StaticFileHandler, {'path': "./images"}),
        (r"/css/(.*)", tornado.web.StaticFileHandler, {'path': "./css"}),
        (r"/js/(.*)", tornado.web.StaticFileHandler, {'path': "./js"}),
        (r"/fonts/(.*)", tornado.web.StaticFileHandler, {'path': "./fonts"}),
    ])

if __name__ == "__main__":
    app = make_app()
    app.listen(8888)
    tornado.ioloop.IOLoop.current().start()
