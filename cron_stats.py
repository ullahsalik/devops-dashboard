from pymongo import MongoClient
import re, os, datetime, time, socket
#import six.moves.urllib as urllib
from subprocess import Popen, PIPE

library_file = "/home/devviacom/cron-library"
mongodb_credentials = "/home/devviacom/mongo-credentials"

def timedelta_total_seconds(timedelta):
    return (
        timedelta.microseconds + 0.0 +
        (timedelta.seconds + timedelta.days * 24 * 3600) * 10 ** 6) / 10 ** 6

def cron_count(stdout,cron):
    #print(stdout)
    count=0	
    for z in stdout.splitlines():
        if re.search(cron,z):
	    #print(z)
	    count+=1	
    #print(count)
    return count

def get_cron_details():
    croncountcmd = ["ps", "-ef"]
    allcmds = {"cron-count": croncountcmd}
    cmd_funcs = {"cron-count": cron_count}
    crondetails = {}
    crons = {}
    count = 0
    f = open(library_file,'r')
    for line in f:
        cron = line.rstrip("\n")
	#print(cron)
        crondetails['cronname'] = cron
        key = "cron{0}".format(count)
	#print(key)
        for cmd in allcmds:
            process = Popen(allcmds[cmd], stdout=PIPE, stderr=PIPE)
            stdout, stderr = process.communicate()
            output = cmd_funcs[cmd](stdout.decode(),cron)
            crondetails[cmd] = output
        count += 1
        crons[key] = crondetails
        crondetails = {}
    return(crons)
doc = {}
doc['count'] = get_cron_details()
#print(doc)	
f = open(mongodb_credentials,'r')
for line in f:
    a = line.split(":")
    host = a[0]
    user = a[1]
    passwd = a[2]
    db_name = a[3].rstrip("\n")
#passwd_parsed = urllib.parse.quote(passwd)
client = MongoClient('mongodb://150.129.146.114:27017/')
db = client['crondata']
db.authenticate(user,passwd)
cronstats = db['cronstats']
doc = {}
time_delta = datetime.datetime.utcnow() - datetime.datetime(1970, 1, 1)
epoch_time = timedelta_total_seconds(time_delta)
doc['epoch_time'] = epoch_time
doc['cron'] = get_cron_details()
#doc['hostname'] = socket.gethostbyname(socket.gethostname())
cronstats.insert_one(doc)
