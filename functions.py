import re, datetime, time, calendar
from pymongo import MongoClient

host = "150.129.146.114"

def get_cron_stats(daterange):
    db_name = "crondata"
    col_name = "cronstats"
    user = "cron_user"
    passw = "wdmongogit@123"
    client = MongoClient('mongodb://150.129.146.114:27017/')
    db = client[db_name]
    db.authenticate(user,passw)
    collection = db[col_name]
    if daterange == None:
        now = datetime.datetime.now()
        start_time = "{0}/{1}/{2}".format(now.month,now.day,now.year)
        start_timestamp = calendar.timegm(time.strptime(start_time,"%m/%d/%Y"))
        end_time = "{0}/{1}/{2} 23:59:59".format(now.month,now.day,now.year)
        end_timestamp = calendar.timegm(time.strptime(end_time,"%m/%d/%Y %H:%M:%S"))
    else:
        start_time = daterange.split()[0]
        start_timestamp = calendar.timegm(time.strptime(start_time,"%m/%d/%Y"))
        end_time = daterange.split()[2] + " 23:59:59"
        end_timestamp = calendar.timegm(time.strptime(end_time,"%m/%d/%Y %H:%M:%S"))
    cursor = collection.find({"epoch_time": {"$gt": start_timestamp, "$lt": end_timestamp}})
    out = {}
    for doc in cursor:
        cname = []
        if 'cron' in doc:
            for count in range(0,len(doc['cron'])):
                key = "cron{0}".format(count)
                if 'cronname' in doc['cron'][key]:
                    cname.append(doc['cron'][key]['cronname'])
                else:
                    cname.append("gibberish")
        break
    for count1 in range(0,len(cname)):
        key1 = "cron{0}".format(count1)
        t = []
        ccount = []
        cursor = collection.find({"epoch_time": {"$gt": start_timestamp, "$lt": end_timestamp}})
        for doc in cursor:
            if 'epoch_time' in doc:
                t.append(time.strftime('%b %d, %y %H:%M',  time.gmtime(doc['epoch_time'])))
                if 'cron' in doc:
                    for count in range(0,len(doc['cron'])):
                        key = "cron{0}".format(count)
                        if 'cronname' in doc['cron'][key] and doc['cron'][key]['cronname'] == cname[count1]:
                            if 'cron-count' in doc['cron'][key]:
                                ccount.append(doc['cron'][key]['cron-count'])
                            else:
                                ccount.append(0)
        out[key1] = {'cronname':cname[count1],'timeseries':t,'count':ccount}
    return out

def get_git_stats(daterange,env):
    collections={"dev":"dp_dev_git","uat":"dp_uat_git","prod":"dp_prod_git"}
    db_name = "gitdata"
    col_name = collections[env]
    user = "git_user"
    passw = "wdmongogit@123"
    client = MongoClient('mongodb://150.129.146.114:27017/')
    db = client[db_name]
    db.authenticate(user,passw)
    collection = db[col_name]
    client.close()
    if daterange == None:
        now = datetime.datetime.now()
        start_time = "{0}/{1}/{2}".format(now.month,now.day,now.year)
        start_timestamp = calendar.timegm(time.strptime(start_time,"%m/%d/%Y"))
        end_time = "{0}/{1}/{2} 23:59:59".format(now.month,now.day,now.year)
        end_timestamp = calendar.timegm(time.strptime(end_time,"%m/%d/%Y %H:%M:%S"))
    else:
        start_time = daterange.split()[0]
        start_timestamp = calendar.timegm(time.strptime(start_time,"%m/%d/%Y"))
        end_time = daterange.split()[2] + " 23:59:59"
        end_timestamp = calendar.timegm(time.strptime(end_time,"%m/%d/%Y %H:%M:%S"))
    cursor = collection.find({"epoch_time": {"$gt": start_timestamp, "$lt": end_timestamp}})
    out = {}
    for doc in cursor:
        sites = []
        if 'site' in doc:
            for count in range(0,len(doc['site'])):
                key = "site{0}".format(count)
                if 'site' in doc['site'][key]:
                    sites.append({'sitename':doc['site'][key]['site'],'path':doc['site'][key]['path'],'git-remote':doc['site'][key]['git-remote']})
                else:
                    sites.append("gibberish")
        break
    for count1 in range(0,len(sites)):
        key1 = "site{0}".format(count1)
        cursor = collection.find({"epoch_time": {"$gt": start_timestamp, "$lt": end_timestamp}})
        stats = []
        for doc in cursor:
            if 'epoch_time' in doc:
                t = time.strftime('%b %d, %y %H:%M',  time.gmtime(doc['epoch_time']))
                if 'site' in doc:
                    for count in range(0,len(doc['site'])):
                        key = "site{0}".format(count)
                        if 'site' in doc['site'][key] and doc['site'][key]['site'] == sites[count1]['sitename']:
                            stats.append({ "time":t,
                                "git-commit": doc['site'][key]['git-commit'],
                                "git-commit-msg": doc['site'][key]['git-commit-msg'],
                                "git-branch": doc['site'][key]['git-branch']
                            })
        out[key1] = {'site':sites[count1]['sitename'],'path':sites[count1]['path'],'git-remote':sites[count1]['git-remote'],'stats':stats}
    return out

def get_timestamp(date_range):
    start_time = date_range.split()[0]
    start_timestamp = time.mktime(datetime.datetime.strptime(start_time, "%m/%d/%Y").timetuple())
    end_time = date_range.split()[2]
    end_timestamp = time.mktime(datetime.datetime.strptime(end_time, "%m/%d/%Y").timetuple())
    timestamp = [start_timestamp, end_timestamp]
    return timestamp
