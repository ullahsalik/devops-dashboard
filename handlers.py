import tornado.web
from functions import *

class IndexHandler(tornado.web.RequestHandler):
    def get(self, url='/'):
        self.render('web/index.html')

class GitHandler(tornado.web.RequestHandler):
    def get(self, url='/gitstats'):
        env = self.get_argument('env')
        a = None
        temp_list = []
        try:
            if self.get_body_argument("daterange"):
                daterange = self.get_body_argument("daterange")
        except:
                daterange = None
        a = get_git_stats(daterange,env)
        temp_list.append(a)
        temp_list.append(env)
        self.render("web/git_display.html", entry=temp_list)

    def post(self):
        a = None
        temp_list=[]
        try:
            if self.get_body_argument("daterange"):
                daterange = self.get_body_argument("daterange")
                env=self.get_body_argument("env")
        except:
                daterange = None
                env=self.get_body_argument("env")
        a = get_git_stats(daterange,env)
        temp_list.append(a)
        temp_list.append(env)
        self.render("web/git_display.html", entry=temp_list)

class CronHandler(tornado.web.RequestHandler):
    def get(self, url='/cronstats'):
        a = None
        try:
            if self.get_body_argument("daterange"):
                daterange = self.get_body_argument("daterange")
        except:
                daterange = None
        a = get_cron_stats(daterange)
        self.render("web/cron_display.html", entry=a)

    def post(self):
        a = None
        try:
            if self.get_body_argument("daterange"):
                daterange = self.get_body_argument("daterange")
        except:
                daterange = None
        a = get_cron_stats(daterange)
        self.render("web/cron_display.html", entry=a)

class FormHandler(tornado.web.RequestHandler):
    def get(self, url='/'):
        self.render('web/form.html')

    def post(self):
        a = get_timestamp(self.get_body_argument("daterange"))
        self.render("web/form1.html", entry=a)
